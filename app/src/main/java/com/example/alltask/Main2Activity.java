package com.example.alltask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView  display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initReference();

        String value = getIntent().getStringExtra("contactTempString");
        display.setText(value);

    }


    void  initReference(){
        display =(TextView) findViewById(R.id.display);
    }
}
