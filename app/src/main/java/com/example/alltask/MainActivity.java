package com.example.alltask;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alltask.Main2Activity;
import com.example.alltask.R;

public class MainActivity extends AppCompatActivity {
    EditText etfirstname,etlastname;
    TextView tvdisplay;
    Button btnok;
    ImageView imgclose;
    private  long backPressedTimme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etfirstname =(EditText) findViewById(R.id.etfirstname);
        etlastname =(EditText) findViewById(R.id.etlastname);
        btnok=(Button)findViewById(R.id.btnok);
        tvdisplay=(TextView)findViewById(R.id.tvdisplay);
        imgclose=(ImageView)findViewById(R.id.imgclose);

        etlastname.getText().toString().trim();
        etlastname.getText().toString().trim();


        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String  firstname =etfirstname.getText().toString();
                String  lastname =etlastname.getText().toString();
                String temp =firstname +" "+ lastname;

                Toast.makeText(getApplicationContext(),temp,Toast.LENGTH_LONG).show();
                Intent h = new Intent(getApplicationContext(), Main2Activity.class);
                String contactTempString = etfirstname.getText().toString() +" " + etlastname.getText().toString();
                h.putExtra("contactTempString",contactTempString);

                startActivity(h);

            }
        });
        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    @Override
    public void onBackPressed() {

        if (backPressedTimme + 2000> System.currentTimeMillis() ){
            super.onBackPressed();
            return;
        }else {
            Toast.makeText(getBaseContext(),"Press Back Again To Exit",Toast.LENGTH_SHORT).show();
        }
        backPressedTimme = System.currentTimeMillis();
    }
}
